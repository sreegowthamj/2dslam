FROM ubuntu:20.04    
# FROM gcc:9.3

ENV DEBIAN_FRONTEND noninteractive

RUN apt-get update
RUN apt-get install -y ca-certificates apt-utils build-essential make libeigen3-dev

# Install relevant dependencies
RUN apt-get install -y cmake

# WORKDIR /code-sample

RUN set -ex;                \
    mkdir -p /usr/src;

COPY . /usr/src/

RUN set -ex;                \
    cd /usr/src/;            \
    cmake . ;               \
    make -j 2 ;                    \
    ./unit_test;




# Here you build your code
# from cmd-line, to build and run - 
# <sudo> docker build -t 2dslam --network=host .