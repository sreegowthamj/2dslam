#include <fcntl.h>
#include <gtest/gtest.h>
#include <stdlib.h>
#include <sys/stat.h>
#include <sys/types.h>

#include <Eigen/Dense>
#include <cassert>
#include <cstdio>
#include <iostream>
#include <random>

#include "Icp.h"
#include "SensorData.h"

#define ANSI_TXT_GRN "\033[0;32m"
#define ANSI_TXT_MGT "\033[0;35m"  // Magenta
#define ANSI_TXT_DFT "\033[0;0m"   // Console default
#define GTEST_BOX "[     cout ] "
#define COUT_GTEST ANSI_TXT_GRN << GTEST_BOX  // You could add the Default
#define COUT_GTEST_MGT COUT_GTEST << ANSI_TXT_MGT

using namespace Eigen;

const std::string dataFilePath("/home/sreegowthamj/ws/2dslam/data/fr079.clf");

// Some basic assertions.
TEST(HelloTest, BasicAssertions) {
  // Expect two strings not to be equal.
  EXPECT_STRNE("hello", "world");
  // Expect equality.
  EXPECT_EQ(7 * 6, 42);
}

TEST(SensorData, LoadSensorData) {
  auto sensorData = SensorData::createInstance(dataFilePath);
  std::cout << "Number of Laser scans collected :"
            << sensorData->getRawSensorData().size() << "\n";
  EXPECT_EQ(sensorData->getRawSensorData().size(), 4934);
}

class IcpTest : public ::testing::Test {
 public:
  const double errorPrecision = 0.0001;
  int numRangeValues = 5000;
  int maxRange = 100;
  SensorSnapshot a, b;
  std::random_device rd;
  std::mt19937 gen{rd()};
  std::normal_distribution<double> distrib =
      std::normal_distribution<double>(0, 0.05);
  double rotationAngle;
  double translationX, translationY;
  Eigen::Rotation2D<double> rotationMat;
  Eigen::Vector2d translationVec;
  IcpTest() {
    a.numberOfRangeValues = numRangeValues;
    a.pose.x = 0;
    a.pose.y = 0;
    a.pose.theta = 0;
    a.ts = 0;
    for (int i = 0; i < numRangeValues; i++) {
      a.rangeData.coordinate.push_back(
          Eigen::Vector2d(rand() % maxRange, rand() % maxRange));
    }

    rotationAngle = distrib(gen);
    translationX = distrib(gen), translationY = distrib(gen);
    //   std::cout << (rotationAngle / 3.14) * 180.00 << "\n";
    //   std::cout << translationX << " " << translationY << "\n";
    rotationMat = Eigen::Rotation2D<double>(rotationAngle);
    translationVec = Eigen::Vector2d(translationX, translationY);
    std::cout << "Rotation matrix : \n"
              << rotationMat.toRotationMatrix() << "\n";
    std::cout << "Translation vec : \n" << translationVec << "\n";

    b.numberOfRangeValues = numRangeValues;
    b.pose.x = translationX;
    b.pose.y = translationY;
    for (auto point : a.rangeData.coordinate) {
      // std::cout << "Element from a: \n" << point << "\n";
      Eigen::Vector2d v = rotationMat * point + translationVec;
      // std::cout << "new element : \n" << v << "\n";
      b.rangeData.coordinate.push_back(v);
    }
  }

  virtual ~IcpTest() {}

  virtual void SetUp() {}

  virtual void TearDown() {}
};

TEST_F(IcpTest, RecoverTranslationAndRotation) {
  Icp icp;

  Transform2D transform2d = icp.recoverTransform(a, b);
  std::cout << "recovered Rotation : \n" << transform2d.first << "\n";
  std::cout << "recovered translation : \n" << transform2d.second << "\n";

  EXPECT_TRUE(transform2d.first.isApprox(rotationMat.toRotationMatrix(),
                                         errorPrecision));
  EXPECT_TRUE(transform2d.second.isApprox(translationVec, errorPrecision));
}

TEST_F(IcpTest, pointwiseSmallestSquaredError) {
  Icp icp;
  Transform2D transform2d = icp.recoverTransform(a, b);
//   std::cout << "recovered Rotation : \n" << transform2d.first << "\n";
//   std::cout << "recovered translation : \n" << transform2d.second << "\n";

  MatchingPointSet mps = icp.pointwiseSmallestSquaredError(a, b, transform2d);
  double absoluteError = 0.00001;
  for( auto x : mps) {
    //   EXPECT_EQ(x.matchingPointIndexSrc, x.pointIndexDst);
    //   EXPECT_NEAR(x.minError, 0.00, absoluteError);
  }

}

int main(int argc, char **argv) {
  ::testing::InitGoogleTest(&argc, argv);
  return RUN_ALL_TESTS();
}