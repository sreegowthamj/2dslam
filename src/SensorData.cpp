#include "SensorData.h"

SharedSensorDataPtr SensorData::data = nullptr;

const SharedSensorDataPtr SensorData::getInstance() {
  assert(SensorData::data);
  return data;
}

const std::vector<SensorSnapshot>& SensorData::getRawSensorData() {
  return rawSensorData;
}

const SharedSensorDataPtr SensorData::createInstance(std::string dataFile) {
  assert(!SensorData::data);
  data = std::make_shared<SensorData>();
  bool status = loadData(dataFile, data);

  return data;
}

SensorData::SensorData() { std::cout << "SensorData::SensorData \n"; }

bool SensorData::loadData(std::string file,
                          SharedSensorDataPtr dataDestination) {
  std::ifstream infile(file.c_str());
  std::string line;
  int dataIndex = 0;
  while (std::getline(infile, line)) {
    SensorSnapshot item;
    std::istringstream iss(line);
    std::string str;
    iss >> str;
    if (str == "FLASER") {
      uint32_t numValues;
      iss >> numValues;
      if (!dataDestination) {
        std::cout << " dataDestination = null \n";
      }
      item.numberOfRangeValues = numValues;
      std::vector<double> rangeValues(numValues, 0);
      for (int i = 0; i < numValues; i++) {
        double rangeVal;
        iss >> rangeVal;
        // item.rangeData.magnitude.push_back(rangeVal);
        float angleStepRadians =
            DEGREE_TO_RADIANS(float(i * (180.0 / numValues) - 90));
        // item.rangeData.angle.push_back(float(DEGREE_TO_RADIANS(angleStepDegrees)));
        item.rangeData.coordinate.push_back(
            Eigen::Vector2d(rangeVal * std::cos(angleStepRadians),
                            rangeVal * std::sin(angleStepRadians)));
        rangeValues[i] = rangeVal;
      }
      double odom_x, odom_y, odom_theta;
      iss >> item.pose.x >> item.pose.y >> item.pose.theta >> odom_x >>
          odom_y >> odom_theta >> item.ts >> str;
      item.rangeData.odomX = odom_x;
      item.rangeData.odomY = odom_y;
      item.rangeData.odomTheta = odom_theta;

    //   for (int i = 0; i < numValues; i++) {
    //     double angleStepRadians =
    //         DEGREE_TO_RADIANS(float(i * (180.0 / numValues) - 90));
    //     Eigen::Vector2d x;
    //     x[0] =
    //         odom_x + rangeValues[i] * std::cos(angleStepRadians + odom_theta);
    //     x[1] =
    //         odom_y + rangeValues[i] * std::sin(angleStepRadians + odom_theta);
    //     item.rangeData.coordinate.push_back(x);
    //   }
      dataIndex++;
      dataDestination->rawSensorData.push_back(item);
    }
  }
  return true;
}