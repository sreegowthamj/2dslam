#ifndef __REGISTER_POINT_CLOUD__
#define __REGISTER_POINT_CLOUD__

#include "SensorData.h"
typedef std::pair<Eigen::Matrix2d, Eigen::Vector2d> Transform2D;


class RegisterPointCloud {
 public:
  virtual Transform2D recoverTransform(SensorSnapshot src, SensorSnapshot dst) = 0;
  virtual ~RegisterPointCloud() {};
};

#endif  //__REGISTER_POINT_CLOUD__