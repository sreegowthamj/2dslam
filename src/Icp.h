#ifndef __Iterative_Closest_Point__
#define __Iterative_Closest_Point__

#include <limits>
#include <numeric>

#include "RegisterPointCloud.h"
#include "SensorData.h"

struct PointCorrespondence {
  int32_t pointIndexDst;
  int32_t matchingPointIndexSrc;
  double minError;
};

typedef std::vector<PointCorrespondence> MatchingPointSet;

class Icp : public RegisterPointCloud {
 public:
  virtual Transform2D recoverTransform(SensorSnapshot src, SensorSnapshot dst);
  MatchingPointSet pointwiseSmallestSquaredError(SensorSnapshot src,
                                                 SensorSnapshot dst, Transform2D transform);
  ~Icp();
};

#endif  //__Iterative_Closest_Point__