#ifndef __SENSOR_DATA__
#define __SENSOR_DATA__

#include <Eigen/Dense>
#include <cassert>
#include <fstream>
#include <iostream>
#include <memory>
#include <sstream>
#include <vector>
#include <cmath>


using namespace Eigen;
class SensorData;

constexpr double pi = 3.14159265358979323846;
#define DEGREE_TO_RADIANS(x)  x * pi/180

typedef double Timestamp;
typedef std::tuple<double, double> RangePoint;
typedef struct {
  std::vector<Eigen::Vector2d> coordinate;
  double odomX;
  double odomY;
  double odomTheta;
} RangeData;
typedef std::shared_ptr<SensorData> SharedSensorDataPtr;

typedef struct {
  double x;
  double y;
  double theta;  // radians
} Pose;

struct SensorSnapshot {
    SensorSnapshot() {
        // std::cout << "sensor snapshot constructor \n";
    }
  RangeData rangeData;
  Timestamp ts;
  Pose pose;
  int32_t numberOfRangeValues;
};

/*
Singleton class for the sensor data
*/
class SensorData {
 private:
  SensorData(const SensorData&) = delete;
  bool loadedData = false;
  static SharedSensorDataPtr data;
  static bool loadData(std::string infile, SharedSensorDataPtr data);
  std::vector<SensorSnapshot> rawSensorData;

 public:
  /*
  TODO: Fix compiler error when constructor is private. Should ideally be
  private
  */
  SensorData();

  const std::vector<SensorSnapshot>& getRawSensorData();
  static const SharedSensorDataPtr createInstance(std::string dataFile);
  static const SharedSensorDataPtr getInstance();
};

#endif //__SENSOR_DATA__