#include <stdlib.h>

#include <Eigen/Dense>
#include <cassert>
#include <cstdio>
#include <iostream>
#include <random>

#include "Icp.h"
#include "SensorData.h"

// TODO: receive this as cmdline param
const std::string dataFilePath("/home/sreegowthamj/ws/2dslam/data/fr079.clf");

using namespace Eigen;

int main() {
  std::cout << "2D slam main\n";
  auto sensorData = SensorData::createInstance(dataFilePath);
  std::cout << "Number of Laser scans collected :"
            << sensorData->getRawSensorData().size() << "\n";
}