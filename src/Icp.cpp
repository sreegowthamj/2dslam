#include "Icp.h"

Transform2D Icp::recoverTransform(SensorSnapshot src, SensorSnapshot dst) {
  assert(src.numberOfRangeValues == dst.numberOfRangeValues);
  Eigen::Vector2d srcMean(0, 0), dstMean(0, 0);
  srcMean = std::accumulate(src.rangeData.coordinate.begin(),
                            src.rangeData.coordinate.end(),
                            Eigen::Vector2d(0.0, 0.0));
  dstMean = std::accumulate(dst.rangeData.coordinate.begin(),
                            dst.rangeData.coordinate.end(),
                            Eigen::Vector2d(0.0, 0.0));

  srcMean /= (double)src.numberOfRangeValues;
  dstMean /= (double)dst.numberOfRangeValues;
  Eigen::MatrixXd S(src.numberOfRangeValues, 2), D(src.numberOfRangeValues, 2);
  for (int i = 0; i < src.numberOfRangeValues; ++i) {
    for (int j = 0; j < 2; ++j)
      S(i, j) = src.rangeData.coordinate[i][j] - srcMean[j];
    for (int j = 0; j < 2; ++j)
      D(i, j) = dst.rangeData.coordinate[i][j] - dstMean[j];
  }
  Eigen::MatrixXd Dt = D.transpose();
  Eigen::Matrix2d H = Dt * S;
  Eigen::Matrix2d W, U, V;
  Eigen::JacobiSVD<Eigen::MatrixXd> svd;
  Eigen::MatrixXd H_(2, 2);
  for (int i = 0; i < 2; ++i)
    for (int j = 0; j < 2; ++j) H_(i, j) = H(i, j);
  svd.compute(H_, Eigen::ComputeThinU | Eigen::ComputeThinV);
  if (!svd.computeU() || !svd.computeV()) {
    std::cerr << "decomposition error" << std::endl;
    return std::make_pair(Eigen::Matrix2d::Identity(), Eigen::Vector2d::Zero());
  }
  Eigen::Matrix2d Vt = svd.matrixV().transpose();
  Eigen::Matrix2d R = svd.matrixU() * Vt;
  Eigen::Vector2d t = dstMean - R * srcMean;
  return std::make_pair(R, t);
}

MatchingPointSet Icp::pointwiseSmallestSquaredError(SensorSnapshot src,
                                                    SensorSnapshot dst,
                                                    Transform2D transform) {
  MatchingPointSet mps;
  int i = 0, j = 0;
  SensorSnapshot srcTransformed = src;
  std::cout << "recovered Rotation : \n" << transform.first << "\n";
  std::cout << "recovered translation : \n" << transform.second << "\n";
  srcTransformed.rangeData.coordinate.clear();
  for (auto point : src.rangeData.coordinate) {
    // std::cout << "Element from a: \n" << point << "\n";
    Eigen::Vector2d v = transform.first * point + transform.second;
    // std::cout << "new element : \n" << v << "\n";
    srcTransformed.rangeData.coordinate.push_back(v);
  }
  double err = 0.0;

  for (auto x : dst.rangeData.coordinate) {
    double minError = std::numeric_limits<double>::max();
    PointCorrespondence pc;
    pc.pointIndexDst = i;
    j = 0;
    for (auto p : srcTransformed.rangeData.coordinate) {
      err = (x - p).norm();
      if (err < minError) {
        pc.matchingPointIndexSrc = j;
        pc.minError = err;
        minError = err;
      }
      j++;
    }
    // std::cout << "Min Error  = " << err << "\n";

    mps.push_back(pc);
    i++;
  }
  return mps;
}

Icp::~Icp() {}