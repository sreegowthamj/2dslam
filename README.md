# 2d SLAM 

## Dataset 
Laser Scan data sets has been downloaded from here -
http://ais.informatik.uni-freiburg.de/slamevaluation/datasets.php

## Dependencies

* Eigen 3.0 or greater
* Google tests (installed automatically by cmake)

## Build 

```
$ git clone https://gitlab.com/sreegowthamj/2dslam.git 2dslam

$ cd 2dslam

$ mkdir build

$ cmake ../

$ make -j4

$ ./unit_test  # runs pre-configured unit tests

$ ./2dslam   # runs the 2d SLAM algorithm

```

# Docker Build & Run

```
$ git clone https://gitlab.com/sreegowthamj/2dslam.git 2dslam

$ cd 2dslam

$ sudo docker build -t 2dslam --network=host 

```
